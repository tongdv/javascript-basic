var square = function (x) {
    return x * x;
};
console.log(square(12));
// → 144


var makeNoise = function () {
    console.log(" Tôi đang bận !");
};
makeNoise();
// → Tôi đang bận !

var power = function (base, exponent) {
    var result = 1;
    for (var count = 0; count < exponent; count++)
        result *= base;
    return result;
};
console.log(power(2, 10));
// → 1024


var x = " outside ";
var f1 = function () {
    var x = " inside f1 ";
};
f1();
console.log(x);
// → outside
var f2 = function () {
    x = " inside f2 ";
};
f2();
console.log(x);
// → inside f2

var landscape = function () {
    var result = "";
    var flat = function (size) {
        for (var count = 0; count < size; count++)
            result += "_";
    };
    var mountain = function (size) {
        result += "/";
        for (var count = 0; count < size; count++)
            result += " ' ";
        result += "\\";
    };
    flat(3); 
    mountain(4);
    flat(6);
    mountain(1);
    flat(1);
    return result;
};
console.log(landscape());
    // → ___ / ' ' ' ' \ ______ / ' \ _

var greeting = "hey hi";
var times = 4;
if (times > 3) {
    var greeting = "say Hello instead"; 
}

console.log(greeting); //"say Hello instead"


var something = 1;
{
    var something = 2;    
}
console.log(something);
// → 2

var something = 1;
{
    let something = 2;    
}
console.log(something);
// → 1

var launchMissiles = function (value) {
    missileSystem.launch(" now ");
};
if (safeMode)
    launchMissiles = function (value) {/* do nothing */ };

function square(x) {
    return x * x;
}

console.log(" The future says :", future());
function future() {
    return "We STILL have no flying cars .";
}

function example() {
    function a() { } // Okay
    if (something) {
        function b() { } // Danger !
    }
}


function greet(who) {
    console.log(" Hello " + who);
}
greet(" Harry ");
console.log(" Bye ");


function chicken() {
    return egg();
}
function egg() {
    return chicken();
}
console.log(chicken() + " came first .");

alert (" Hello ", " Good Evening ", "How do you do ?");

function power(base, exponent) {
    if (exponent == undefined)
        exponent = 2;
    var result = 1;
    for (var count = 0; count < exponent; count++)
        result *= base;
    return result;
}
console.log(power(4));
// → 16
console.log(power(4, 3));
// → 64

function wrapValue(n) {
    var localVariable = n;
    return function () { 
        return localVariable; 
    };
}
var wrap1 = wrapValue(1);
var wrap2 = wrapValue(2);
console.log(wrap1());
// → 1
console.log(wrap2());
// → 2


function outside(x) {
    function inside(y) {
        return x + y;
    }
    return inside;
}
fn_inside = outside(3);
result = fn_inside(5); // #=> 8

result1 = outside(3)(5); // #=> 8


function power(base, exponent) {
    if (exponent == 0)
        return 1;
    else
        return base * power(base, exponent - 1);
}
console.log(power(2, 3));
// → 8

function printFarmInventory(cows, chickens) {
    // In ra số bò
    var cowString = String(cows);
    while (cowString.length < 3)
        cowString = "0" + cowString;
    console.log(cowString + " bò");
    // In ra số gà
    var chickenString = String(chickens);
    while (chickenString.length < 3)
        chickenString = "0" + chickenString;
    console.log(chickenString + " gà");
}
printFarmInventory(7, 11);

function printZeroPaddedWithLabel(number, label) {
    var numberString = String(number);
    while (numberString.length < 3)
        numberString = "0" + numberString;
    console.log(numberString + " " + label);
}
function printFarmInventory(cows, chickens, pigs) {
    printZeroPaddedWithLabel(cows, " Cows ");
    printZeroPaddedWithLabel(chickens, " Chickens ");
    printZeroPaddedWithLabel(pigs, " Pigs ");
}
printFarmInventory (7, 11, 3);

function zeroPad(number, width) {
    var string = String(number);
    while (string.length < width)
        string = "0" + string;
    return string;
}
function printFarmInventory(cows, chickens, pigs) {
    console.log(zeroPad(cows, 3) + " Cows ");
    console.log(zeroPad(chickens, 3) + " Chickens ");
    console.log(zeroPad(pigs, 3) + " Pigs ");
}
printFarmInventory(7, 16, 3);

var listOfNumbers = [2, 3, 5, 7, 11];
console .log( listOfNumbers [2]) ;
// → 5
console .log( listOfNumbers [2 - 1]);
// → 3

var doh = "Doh ";
console .log( typeof doh. toUpperCase );
// → function
console .log(doh. toUpperCase ());
// → DOH

var mack = [];
mack . push ("Mack");
mack . push ("the", "Knife");
console .log( mack );
// → [" Mack ", "the", " Knife "]
console .log( mack . join (" "));
// → Mack the Knife
console .log( mack .pop ());
// → Knife
console .log( mack );
// → [" Mack ", "the "]

var day1 = {
    squirrel: false,
    events: ["work", "touched tree", "pizza", "running", "television"]
};

console .log( day1 . squirrel );
// → false
console .log( day1 . wolf );
// → undefined
day1 . wolf = false ;
console .log( day1 . wolf );
// → false

var descriptions = {
    work: " Went to work ",
    " touched tree ": " Touched a tree "
};


var journal = [
    {
        events: [" work ", " touched tree ", " pizza ",
            " running ", " television "],
        squirrel: false
    },
    {
        events: [" work ", "ice cream ", " cauliflower ",
            " lasagna ", " touched tree ", " brushed teeth "],
        squirrel: false
    },
    {
        events: [" weekend ", " cycling ", " break ",
            " peanuts ", " beer "],
        squirrel: true
    }   
];

var map = {};
function storePhi(event, phi) {
    map[event] = phi;
}
storePhi(" pizza ", 0.069);
storePhi(" touched tree ", -0.081);
console.log(" pizza " in map);
// → true
console.log(map[" touched tree "]);
// → -0.081

for (var event in map)
    console .log (" The correlation for ' " + event + " ' is " + map[ event ]);
// → The correlation for ' pizza ' is 0.069
// → The correlation for ' touched tree ' is -0.081

var todoList = [];
function rememberTo(task) {
    todoList.push(task);
}
function whatIsNext() {
    return todoList.shift();
}
function urgentlyRememberTo(task) {
    todoList.unshift(task);
}

console .log ([1 , 2, 3, 2, 1]. indexOf (2));
// → 1
console .log ([1 , 2, 3, 2, 1]. lastIndexOf (2));
// → 3

function remove(array, index) {
    return array.slice(0, index)
        .concat(array.slice(index + 1));
}
console.log(remove(["a", "b", "c", "d", "e"], 2));
    // → ["a", "b", "d", "e"]

var myString = " Fido ";
myString.myProperty = " value ";
console.log(myString.myProperty);
// → undefined

console .log (" coconuts ". slice (4, 7));
// → nut
console .log (" coconut ". indexOf ("u"));
// → 5

console .log (" okay \n ". trim ());
// → okay

var string = "abc ";
console .log( string . length );
// → 3
console .log( string . charAt (0));
// → a
console .log( string [1]) ;
// → b

function noArguments() { }
noArguments(1, 2, 3); // This is okay
function threeArguments(a, b, c) { }
threeArguments(); // And so is this

function argumentCounter() {
    console.log(" You gave me", arguments.length, " arguments .");
}
argumentCounter(" Straw man", " Tautology ", "Ad hominem ");
    // → You gave me 3 arguments .

addEntry([" work ", " touched tree ", " pizza ", " running ",
    " television "], false);

function addEntry(squirrel) {
    var entry = { events: [], squirrel: squirrel };

    for (var i = 1; i < arguments.length; i++)
        entry.events.push(arguments[i]);
    journal.push(entry);
}
addEntry(true, " work ", " touched tree ", " pizza ",
    " running ", " television ");

var myVar = 10;
console.log(" myVar " in window);
// → true
console.log(window.myVar);
    // → 10
